export default ({ app }, inject) => {
  const datePlugin = {
    convertFirestoreTimestampToDate: timestamp => {
      try {
        if (timestamp.seconds) {
          return new Date(timestamp.seconds * 1000);
        }
        return timestamp;
      } catch (e) {
        return null;
      }
    },
  };

  inject("date", datePlugin);
};
