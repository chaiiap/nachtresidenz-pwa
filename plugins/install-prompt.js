import Cookies from "js-cookie";

window.addEventListener("beforeinstallprompt", e => {
  // Prevent default install prompt
  e.preventDefault();
  // Stash the event so it can be triggered later.
  if (Cookies.get("nac-install-dismissed") === undefined) {
    window.deferredPrompt = e;
  }
});

window.addEventListener("appinstalled", () => {
  window.deferredPrompt = null;
});
