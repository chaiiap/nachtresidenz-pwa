export default function({ store, redirect }) {
  if (store.state.authUser) {
    console.log("[auth]: Already logged in");
    return redirect("/membercard");
  }
}
