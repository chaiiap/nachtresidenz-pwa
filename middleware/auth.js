export default function({ store, redirect }) {
  if (!store.state.authUser) {
    console.log("[auth]: Unauthorized");
    return redirect("/login");
  }
}
