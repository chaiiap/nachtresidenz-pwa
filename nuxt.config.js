export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "Nachtresidenz App",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon3.ico" },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600&family=PT+Sans:wght@400;700&display=swap",
      },
    ],
  },

  loading: "@/components/loading.vue",

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "~/plugins/vue-tel-input.js", ssr: false },
    { src: "~/plugins/vuejs-datepicker.js", ssr: false },
    { src: "~/plugins/install-prompt.js", ssr: false },
    { src: "~/plugins/vue-html2pdf.js", ssr: false },
    { src: "~/plugins/date.js" },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    "@nuxtjs/style-resources",
    [
      "@nuxtjs/fontawesome",
      {
        component: "fa",
        icons: {
          brands: ["faSquareWhatsapp"],
        },
        proIcons: {
          light: [
            "faSpinner",
            "faChevronLeft",
            "faChevronRight",
            "faCircleCheck",
            "faLightbulbOn",
            "faCircleInfo",
            "faXmark",
          ],
          solid: ["faSquareCheck", "faChevronRight"],
          regular: ["faSquare"],
        },
      },
    ],
    "@nuxtjs/moment",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    "@nuxtjs/axios",
    // https://go.nuxtjs.dev/pwa
    "@nuxtjs/pwa",
    [
      "@nuxtjs/firebase",
      {
        config: {
          apiKey: process.env.FIREBASE_API_KEY,
          authDomain: process.env.FIREBASE_AUTH_DOMAIN,
          databaseURL: process.env.FIREBASE_DATABASE_URL,
          projectId: process.env.FIREBASE_PROJECT_ID,
          storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
          messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
          appId: process.env.FIREBASE_APP_ID,
        },
        services: {
          auth: {
            persistence: "local",
            initialize: {
              // onAuthStateChangedMutation: "SET_USER",
              onAuthStateChangedAction: "onAuthStateChangedAction",
            },
            // ssr: false,
            ssr: {
              serverLogin: {
                sessionLifetime: 60 * 60 * 1000,
                loginDelay: 50,
              },
            },
            emulatorPort: process.env.NODE_ENV === "development" ? 9099 : false,
            emulatorHost: "http://localhost",
          },
          firestore: {
            emulatorPort: process.env.NODE_ENV === "development" ? 8080 : false,
            emulatorHost: "localhost",
          },
          storage: {
            emulatorPort: process.env.NODE_ENV === "development" ? 9199 : false,
            emulatorHost: "127.0.0.1",
          },
        },
      },
    ],
    [
      "@nuxtjs/recaptcha",
      {
        hideBadge: false,
        language: "de",
        version: 3,
        size: "invisible",
      },
    ],
    "nuxt-facebook-pixel-module",
  ],

  publicRuntimeConfig: {
    recaptcha: {
      siteKey: process.env.RECAPTCHA_SITE_KEY,
    },
  },

  privateRuntimeConfig: {
    axios: {
      baseURL: process.env.API_URL,
    },
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: "/",
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    meta: true,
    icon: {
      source: "~/static/icon3.jpg",
      fileName: "icon3.jpg",
    },
    workbox: {
      importScripts: ["/firebase-auth-sw.js"],
      runtimeCaching: [
        {
          urlPattern: "https://fonts.googleapis.com/.*",
          handler: "cacheFirst",
          method: "GET",
          strategyOptions: { cacheableResponse: { statuses: [0, 200] } },
        },
      ],
      dev: process.env.NODE_ENV === "development",
    },
    manifest: {
      name: "Nachtresidenz App",
      short_name: "Nachtresidenz",
      lang: "de",
      display: "fullscreen",
      background_color: "#ffffff",
      theme_color: "#202020",
    },
  },

  moment: {
    locales: ["de"],
    timezone: true,
  },

  facebook: {
    debug: true,
    autoPageView: true,
    pixelId: "703785821544369",
    pixels: [
      {
        pixelId: "444861857203405",
        routes: ["/danke"],
      },
      {
        pixelId: "1265668154333429",
        routes: ["/verify"],
      },
    ],
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  styleResources: {
    scss: ["~/assets/styles/main.scss"],
  },

  env: {
    backendApiUrl: process.env.BACKEND_API_URL,
  },
};
