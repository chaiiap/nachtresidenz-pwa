export const state = () => ({
  authUser: null
});

export const getters = {
  currentUser: state => state.authUser,
  isLoggedIn: state => !!state.authUser
};

export const actions = {
  async onAuthStateChangedAction({ commit }, { authUser, claims }) {
    if (!authUser) {
      commit("LOGOUT");
    } else {
      const { uid, email, emailVerified, displayName, photoURL } = authUser;
      commit("SET_USER", {
        uid,
        email,
        emailVerified,
        displayName,
        photoURL,
        isAdmin: claims.custom_claim
      });
    }
  },
  async nuxtServerInit({ dispatch, commit }, { res }) {
    if (res && res.locals && res.locals.user) {
      const {
        allClaims: claims,
        idToken: token,
        ...authUser
      } = res.locals.user;
      await dispatch("onAuthStateChangedAction", {
        authUser,
        claims,
        token
      });
      commit("ON_AUTH_STATE_CHANGED_MUTATION", { authUser, claims, token });
    }
  },
  async logout({ commit }) {
    commit("LOGOUT");
  }
};

export const mutations = {
  ON_AUTH_STATE_CHANGED_MUTATION(state, { authUser, claims }) {
    if (!authUser) {
      // perform logout operations
      state.authUser = null;
    } else {
      const { uid, email, emailVerified, displayName, photoURL } = authUser;
      state.authUser = {
        uid,
        displayName,
        email,
        emailVerified,
        photoURL: photoURL || null,
        isAdmin: claims.custom_claim
      };
    }
  },
  SET_USER(state, payload) {
    state.authUser = payload;
  },
  LOGOUT(state) {
    state.authUser = null;
  }
};
